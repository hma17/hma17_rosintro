%lengths of links found from urdf 
l1 = 0.089159;
l2 = 0.425;
l3 = 0.39225; 
l4 = 0.10915;
l5 = 0.09465;
l6 = 0.0823; 

%a values 
a1 = 0; 
a2 = l2;
a3 = l3;
a4 = 0; 
a5 = 0; 
a6 = 0;

%d values 
d1 = l1;
d2 = 0;
d3 = 0; 
d4 = l4;
d5 = l5; 
d6 = 0; 

%absolute value of theta values from get_data file
q1 = 2.790907464595611;
q2 = 1.348426954367942;
q3 = 1.8162850987755714; 
q4 = 3.1180465015101717; 
q5 = 5.063917250853007; 
q6 = 3.140749506999269; 

%alpha values from urdf
al1 = pi/2;
al2 = 0;
al3 = 0; 
al4 = pi/2;
al5 = pi/2;
al6 = 0; 

T0 = [1 0 0 0; 
      0 1 0 0; 
      0 0 1 0;
      0 0 0 1];

T1 = [cos(q1) -sin(q1) 0 0;
      sin(q1) cos(q1) 0 0;
      0 0 1 l1; 
      0 0 0 1] * [1 0 0 a1; 0 cos(al1) -sin(al1) 0; 0 sin(al1) cos(al1) 0; 0 0 0 1]; 

  
T2 = [cos(q2) -sin(q2) 0 0;
      sin(q2) cos(q2) 0 0;
      0 0 1 l1; 
      0 0 0 1] * [1 0 0 a2; 0 cos(al2) -sin(al2) 0; 0 sin(al2) cos(al2) 0; 0 0 0 1]; 
  
T3 = [cos(q3) -sin(q3) 0 0;
      sin(q3) cos(q3) 0 0;
      0 0 1 l1; 
      0 0 0 1] * [1 0 0 a3; 0 cos(al3) -sin(al3) 0; 0 sin(al3) cos(al3) 0; 0 0 0 1]; 
  
T4 = [cos(q4) -sin(q4) 0 0;
      sin(q4) cos(q4) 0 0;
      0 0 1 l1; 
      0 0 0 1] * [1 0 0 a1; 0 cos(al4) -sin(al4) 0; 0 sin(al4) cos(al4) 0; 0 0 0 1]; 
  
  
T5 = [cos(q5) -sin(q5) 0 0;
      sin(q5) cos(q5) 0 0;
      0 0 1 l1; 
      0 0 0 1] * [1 0 0 a5; 0 cos(al5) -sin(al5) 0; 0 sin(al5) cos(al5) 0; 0 0 0 1]; 
  
T6 = [cos(q1) -sin(q1) 0 0;
      sin(q1) cos(q1) 0 0;
      0 0 1 l1; 
      0 0 0 1] * [1 0 0 a1; 0 cos(al6) -sin(al6) 0; 0 sin(al6) cos(al6) 0; 0 0 0 1]; 
    
A = T1 * T2 * T3 * T4 * T5 * T6;
disp("A Matrix: ")
disp(A) 

%jacobian matrix
%identity matrix, 3rd column x 
%O - 4th column 
On = T6(:,4);
On(4,:) = [];

Oi_1 = T5(:,4);
Oi_1(4,:) = [];
O = On - Oi_1; 

Zi_1 = T5(:,3); 
Zi_1(4,:) = []; 

J1 = cross(Zi_1, O);
disp(J1);

